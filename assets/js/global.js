/* Author:http://www.rainatspace.com*/

function initializeScript(){

	//TABS
	jQuery(".tab_content:first").show();
	jQuery("#tabs li").click(function() {
		jQuery("#tabs li").removeClass('active');
		jQuery(this).addClass("active");
		jQuery(".tab_content").hide();
		var selected_tab = jQuery(this).find("a").attr("href");
		jQuery(selected_tab).fadeIn();
		return false;
	});

	//MODAL POPUP
	var appendthis =  ("<div class='modal-overlay js-modal-close'></div>");
	jQuery('a[data-modal-id]').click(function(e) {
		e.preventDefault();
		jQuery("body").append(appendthis);
		jQuery(".modal-overlay").fadeTo(500, 0.7);
		//$(".js-modalbox").fadeIn(500);
		var modalBox = $(this).attr('data-modal-id');
		jQuery('#'+modalBox).fadeIn($(this).data());
	});  
 
	jQuery(".js-modal-close, .modal-overlay").click(function() {
	    jQuery(".modal-box, .modal-overlay").fadeOut(500, function() {
	        jQuery(".modal-overlay").remove();
	    });
	 
	});
	jQuery(window).resize(function() {
	    jQuery(".modal-box").css({
	        top: (jQuery(window).height() - jQuery(".modal-box").outerHeight()) / 2,
	        left: (jQuery(window).width() - jQuery(".modal-box").outerWidth()) / 2
	    });
	});
	$(window).resize();
}

/* =Document Ready Trigger
-------------------------------------------------------------- */
jQuery(document).ready(function(){
    initializeScript();
});
/* END ------------------------------------------------------- */